package com.example.canaldataes.es.service;

import com.example.canaldataes.es.entity.EsData;
import com.example.canaldataes.es.repository.EsDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EsDataService{

    @Autowired
    private EsDataRepository esDataRepository;

    public void saveData(EsData esData){
        esDataRepository.save(esData);
    }
}
