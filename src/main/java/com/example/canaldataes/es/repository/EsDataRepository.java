package com.example.canaldataes.es.repository;

import com.example.canaldataes.es.entity.EsData;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EsDataRepository extends ElasticsearchRepository<EsData, String> {
}