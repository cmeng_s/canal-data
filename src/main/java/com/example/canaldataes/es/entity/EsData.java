package com.example.canaldataes.es.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import java.util.List;

@Data
@Document(indexName = "canal-data")
public class EsData {

    @Id
    private String id;

    private String table;

    private String type;

    private String sql;

    private List old;

    private String database;

    private List data;
}
