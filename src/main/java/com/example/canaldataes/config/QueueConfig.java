package com.example.canaldataes.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "spring.rabbitmq.config.canal")
public class QueueConfig {


    private String routerKey;


    private String exchange;


    private String queueName;

}
