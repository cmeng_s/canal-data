package com.example.canaldataes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CanalDataEsApplication {

    public static void main(String[] args) {
        SpringApplication.run(CanalDataEsApplication.class, args);
    }

}
